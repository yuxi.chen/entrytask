package main

import (
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	"fmt"
	"hash/fnv"
	"math/rand"
	"net"
	"strings"
)

var db *sql.DB

type user struct {
	user_id string
	password string
}
func initDB(id string, pwd string, operation string) (res string, err error) {
	dsn := "root:@tcp(127.0.0.1:3306)/entrytask"
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		panic(err)
	}
	err = db.Ping()
	if err != nil {
		panic(err)
	}
	if operation == "login" {
		var u user
		fmt.Println("id: ", id,", password: ", pwd,", operation: ",operation)
		sqlStr:="select * from user where user_id = ?"
		rows,err:=db.Query(sqlStr, id)
		defer rows.Close()
		if !rows.Next(){
			return "False", err
		} else{
			rows.Scan(&u.user_id,&u.password)
			S := Split_string(u.password)
			salt := S[0]
			h := fnv.New32a()
			h.Write([]byte(u.password+salt))
			if u.password==string(h.Sum32()){
				return "True", err
			} else{
				return "False", err
			}
		}
	} else {         //register
		salt := string(rand.Int())
		h := fnv.New32a()
		h.Write([]byte(pwd+salt))
		pwd = salt + ":" + string(h.Sum32())

		sqlStr:="select * from user where user_id = ?"
		fmt.Println("id: ", id,", password: ", pwd,", operation: ",operation)
		rows,err:=db.Query(sqlStr, id)
		defer rows.Close()
		if !rows.Next() {
			sqlStr:="insert into user(user_id, password) value(?,?)"
			_,err = db.Exec(sqlStr, id, pwd)
		} else {
			return "False",err
		}
	}
	return "True", err
}
func main() {
	fmt.Println("Starting the server ...")
	listener, err := net.Listen("tcp", "127.0.0.1:50008")
	if err != nil {
		panic(err)
		return
	}
	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("Error accepting", err.Error())
			return
		}
		Process(conn)
	}
	db.Close()
}


func Process(conn net.Conn) {
	buf := make([]byte, 1024)
	reply := []byte("")
	length, err := conn.Read(buf)
	if err != nil {
		panic(err)
		reply = []byte("Error reading")
		return
	}
	S := Split_string(string(buf[:length]))     //decode the message  user_id:password:operation
	user_id := S[0]
	password := S[1]
	operation := S[2]
	result, err:=initDB(user_id,password, operation)
	if err!= nil {
		panic(err)
		return
	}
	reply = []byte(result)
    conn.Write(reply)
}

func split(s rune) bool{
	return s==':'
}
func Split_string(s string) []string {
	a:=strings.FieldsFunc(s, split)
	return a
}